//Michael Obadia
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
public class TestFibonacci {
    @Test
    public void test2ndTerm() {
        FibonacciSequence f = new FibonacciSequence(4,5);
        assertEquals(f.getTerm(1), 5);
    }
    @Test
    public void test3rdTerm() {
        FibonacciSequence f = new FibonacciSequence(4,5);
        assertEquals(f.getTerm(2), 9);
    }
    @Test
    public void test5thTerm() {
        FibonacciSequence f = new FibonacciSequence(4,5);
        assertEquals(f.getTerm(4), 23);
    }
    @Test
    public void test10thTerm() {
        FibonacciSequence f = new FibonacciSequence(4,5);
        assertEquals(f.getTerm(9), 254);
    }

    @Test
    public void testNegative() {
        try {
            FibonacciSequence f = new FibonacciSequence(-3,5);
            assertEquals(f.getTerm(-4), 23);
            fail("Error not catched");
        }
        catch (IllegalArgumentException e){
            //if error gets catched, test succeeds
        }
    }

}

