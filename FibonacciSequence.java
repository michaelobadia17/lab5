//Johnny Hoang
public class FibonacciSequence extends Sequence{

private int first;
private int second;

    public FibonacciSequence(int first, int second){
        this.first = first;
        this.second = second;
    }

    public int getTerm(int n){
        if (n<0){
            throw new IllegalArgumentException("Index needs to be positive.");
        }
        final int MIN_SPACE = 2;
        int [] sequence = new int[n+MIN_SPACE];

        sequence[0]=this.first;
        sequence[1]=this.second;

        if (n==1 || n==0){
            return sequence[n];
        }
        else {
            for (int i=MIN_SPACE; i<sequence.length; i++){
                sequence[i]=sequence[i-1]+ sequence[i-2]; 
            }
            return sequence[n];
       }  
     }
}
