//Johnny Hoang
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
public class SkiponacciTest {
    @Test
    public void getTermThirdTerm(){
        SkiponacciSequence s = new SkiponacciSequence(3,4,5);
        assertEquals(5,s.getTerm(2));
    }
    @Test
    public void getTermTestSecondTerm(){
        SkiponacciSequence s = new SkiponacciSequence(2,3,4);
        assertEquals(3,s.getTerm(1));
    }
    @Test
    public void getTermTestTenthTerm(){
        SkiponacciSequence s = new SkiponacciSequence(2,3,4);
        assertEquals(28,s.getTerm(9));
    }
    @Test
    public void getTermTestNegative(){
        try {
            SkiponacciSequence s = new SkiponacciSequence(2,3,4);
            assertEquals(3,s.getTerm(-1));
            fail("No error catched");
        }
        catch (IllegalArgumentException e){
            //if error gets catched, test succeeds
        }
    }
}
