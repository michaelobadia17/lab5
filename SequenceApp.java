import java.util.*;
public class SequenceApp {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        String s = reader.next();
        Sequence[] a = parse(s);
        print(a, 10);
    }   

    public static Sequence[] parse(String s) {
        int arrLength = 0;
        for(int i = 0; i < s.length(); i++) {
            if(s.charAt(i) == 'x') {
                arrLength++;
            }
        }
        Sequence[] sArr = new Sequence[arrLength];
        int start = 0;
        String[] splitSeq = s.split(";");
        for(int j = 0; j < splitSeq.length; j++) {
            if(splitSeq[j] .equals("Fib")) {
                sArr[start] = new FibonacciSequence(Integer.parseInt(splitSeq[j+1]), Integer.parseInt(splitSeq[j+2]));
                start++;
            }
            else if(splitSeq[j]  .equals("Skip")) {
                sArr[start] = new SkiponacciSequence(Integer.parseInt(splitSeq[j+1]), Integer.parseInt(splitSeq[j+2]), Integer.parseInt(splitSeq[j+3]));
                start++;
            }
            else {
            }
        }
        return sArr;
    }

    public static void print(Sequence[] sequences, int n){
        for(int i = 0; i < sequences.length; i++) {
            for(int j = 0; j < n; j++) {
                System.out.println((j + 1) + ". " + sequences[i].getTerm(j));
            }
            System.out.println("---------------");
        }
    }
}
