//Michael Obadia
public class SkiponacciSequence extends Sequence {
    private int n1;
    private int n2;
    private int n3;
    public SkiponacciSequence(int n1, int n2, int n3) {
        this.n1 = n1;
        this.n2 = n2;
        this.n3 = n3;
    }

    public int getTerm(int n) {
        if(n<0) {
            throw new IllegalArgumentException("Index needs to be positive.");
        }
        int[] sArr = new int[n + 3];
        sArr[0] = this.n1;
        sArr[1] = this.n2;
        sArr[2] = this.n3;
        int sum = sArr[n];
        if(n > 1) {
            for(int i = 2; i < sArr.length; i++) {
            sum = sArr[i - 1] + sArr[i - 2];
            if(i < sArr.length - 1) {
            sArr[i + 1] = sum;
            }
            }
            sum = sArr[n];
        }
        return sum;
    }
}
